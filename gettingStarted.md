_Getting Started with Git_

Was Word nicht kann, ist verschiedene Versionen von Text gleichzeitig zu verwalten. Deswegen will ich ermuntern, Text ein wenig nerdiger zu schreiben und dadurch an Sicherheit im Schreibprozess zu gewinnen. Es geht nichts kaputt, man kann immer zurück und es liegt immer nicht nur eine sichere Kopie im Internet, sondern eine Kopie aler Arbeitsstände.

Der erste Beispieltext für das Git-Repo soll daher dakumentieren, dass es wirklich Sinn macht, mutig zu sein und eine klare "Separation of Concerns" auch bei Schreiben einzuführen. 

* zunächst das Schreiben selbst
* dann das Layouten
* und schließlich Kompilieren und Rekompilieren als Methoden, den Inhalt besser zu machen oder wiederzuverwenden

Und hier kommt dann das obligorische Lorem Ipsum, ne!