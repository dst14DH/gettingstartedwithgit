# README #

Dieses Repo begleitet meine Git-Einführung für (Noch) Nicht-Programmierer.

Mehr dazu im Blog-Artikel: [git-Einstieg](http://blog.danielstange.de/textverarbeitung-und-git/)

# Links 
Die Musterdatei im Projekt wurde mit Scrivener erstellt, das man für Mac und Windows [bei Literature and Latte herunterladen und 30 Tage kostenlos testen kann](https://www.literatureandlatte.com/). Für 40$ ist das Programm nicht wirklich teuer und definitiv ein richtig gute Wahl, um Manuskripte zu verfassen.